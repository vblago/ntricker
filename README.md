# Tricker

NURE project

I update Tricker with new features and bugs fixes, but the apk may be behind master. Take a look at the [changelog](/CHANGELOG.md) to see the most recent additions to the apk.


## Features

 * Custom photo feed based on who you follow (using firebase cloud functions)

## Dependencies

* [Firestore](https://github.com/flutter/plugins/tree/master/packages/cloud_firestore) - cloud storage of entities and images, as well as a backend for processing photos and issuing relevant posts for a specific user
* [Image Picker](https://github.com/flutter/plugins/tree/master/packages/image_picker) - plugin for native access to camera and gallery functions for issuing a finished photo in bit format
* [Google Sign In](https://github.com/flutter/plugins/tree/master/packages/google_sign_in) - plug-in for authorization service through google account
* [Firebase Auth](https://github.com/flutter/plugins/tree/master/packages/firebase_auth) - plug-in layer for the service of storing authorization keys
* [UUID](https://github.com/Daegalus/dart-uuid) - plugin to get a unique device identifier
* [Path Provider](https://github.com/flutter/plugins/tree/master/packages/path_provider) - plug-in for managing location in the file system of a mobile device of temporarily stored photos
* [Font Awesome](https://github.com/brianegan/font_awesome_flutter) - plugin for using icon pack
* [Dart Http](https://github.com/dart-lang/http) - plugin for making https requests
* [Flutter Shared Preferences](https://github.com/flutter/plugins) - plugin for saving basic information and primitives on a mobile device
* [Cached Network Image](https://github.com/renefloor/flutter_cached_network_image) - plugin for caching some photos

## Getting started


#### 1. [Setup Flutter](https://flutter.io/setup/)

#### 2. Clone the repo

```sh
$ git clone https://gitlab.com/vblago/tricker.git
$ cd tricker/
```

#### 3. Setup the firebase app

1. You'll need to create a Firebase instance. Follow the instructions at https://console.firebase.google.com.
2. Once your Firebase instance is created, you'll need to enable anonymous authentication.

* Go to the Firebase Console for your new instance.
* Click "Authentication" in the left-hand menu
* Click the "sign-in method" tab
* Click "Google" and enable it

3. Create Cloud Functions (to make the Feed work)
* Create a new firebase project with `firebase init`
* Copy this project's `functions/lib/index.js` to your firebase project's `functions/index.js`
* Push the function `getFeed` with `firebase deploy --only functions`  In the output, you'll see the getFeed URL, copy that.
* Replace the url in the `_getFeed` function in `feed.dart` with your cloud function url from the previous step.


_**If this does not work**  and you get the error `Error: Error parsing triggers: Cannot find module './notificationHandler'` Try following [these steps](https://github.com/mdanics/fluttergram/issues/25#issuecomment-434031430). If you are still unable to get it to work please open a new issue._

_**If you are getting no errors, but an empty feed** You must follow users with posts as the getFeed function only returns posts from people you follow._



4. Enable the Firebase Database
* Go to the Firebase Console
* Click "Database" in the left-hand menu
* Click the Cloudstore "Create Database" button
* Select "Start in test mode" and "Enable"

5. (skip if not running on Android)

* Create an app within your Firebase instance for Android, with package name com.yourcompany.news
* Run the following command to get your SHA-1 key:

```
keytool -exportcert -list -v \
-alias androiddebugkey -keystore ~/.android/debug.keystore
```

* In the Firebase console, in the settings of your Android app, add your SHA-1 key by clicking "Add Fingerprint".
* Follow instructions to download google-services.json
* place `google-services.json` into `/android/app/`.


6. (skip if not running on iOS)

* Create an app within your Firebase instance for iOS, with your app package name
* Follow instructions to download GoogleService-Info.plist
* Open XCode, right click the Runner folder, select the "Add Files to 'Runner'" menu, and select the GoogleService-Info.plist file to add it to /ios/Runner in XCode
* Open /ios/Runner/Info.plist in a text editor. Locate the CFBundleURLSchemes key. The second item in the array value of this key is specific to the Firebase instance. Replace it with the value for REVERSED_CLIENT_ID from GoogleService-Info.plist

Double check install instructions for both
   - Google Auth Plugin
     - https://pub.dartlang.org/packages/firebase_auth
   - Firestore Plugin
     -  https://pub.dartlang.org/packages/cloud_firestore
